package com.cliente.cliente.to;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "pessoa")
@Inheritance(strategy = InheritanceType.JOINED)
public class Pessoa {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pessoa")
	private int idPessoa;
	
	@NotBlank(message="O campo 'Nome' não pode esta vazio!")
	@NotEmpty(message="O campo 'Nome' não pode esta vazio!")
	@NotNull(message="O campo 'Nome' não pode esta vazio!")
	@Size(max = 80)
	@Column(name = "nome", length = 80)
	private String nome;
	
	@NotNull
	@Column(name = "id_tipo_pessoa", length = 1)
	private int idTipoPessoa;
	
	@OneToOne(mappedBy = "pessoa", cascade = CascadeType.ALL, fetch = FetchType.LAZY)     
    private Cliente cliente;
	
	public Pessoa() {
		super();
	}
	public Pessoa(int idPessoa, String nome) {
		super();
		this.idPessoa = idPessoa;
		this.nome = nome;
	}
	public int getIdPessoa() {
		return idPessoa;
	}
	public void setIdPessoa(int idPessoa) {
		this.idPessoa = idPessoa;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdTipoPessoa() {
		return idTipoPessoa;
	}
	public void setIdTipoPessoa(int idTipoPessoa) {
		this.idTipoPessoa = idTipoPessoa;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}
