package com.cliente.cliente.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cliente.cliente.to.Cliente;
import com.cliente.cliente.to.PessoaFisica;
import com.cliente.cliente.to.PessoaJuridica;

@Repository
public interface RepositorioCliente extends JpaRepository<Cliente, Integer> {
	
	@Query(value = "SELECT PF.CPF,PF.ID_PESSOA FROM PESSOA_FISICA PF WHERE PF.CPF = :CPF", nativeQuery = true)
	PessoaFisica buscaPorCPF(@Param("CPF") String cpf);
	
	@Query(value = "SELECT PJ.CNPJ,PJ.ID_PESSOA FROM PESSOA_FISICA PJ WHERE PJ.CNPJ = :CNPJ", nativeQuery = true)
	PessoaJuridica buscaPorCNPJ(@Param("CNPJ") String cnpj);

}
